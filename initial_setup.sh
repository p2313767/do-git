#!/bin/sh
# Vérifie la présence de la clé publique
while [ ! -f /root/.ssh_transfer/id_rsa.pub ]; do
  sleep 1
done
# Copie la clé dans authorized_keys
cat /root/.ssh_transfer/id_rsa.pub >> /home/git/.ssh/authorized_keys
service ssh start
tail -f /dev/null


