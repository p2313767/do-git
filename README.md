DO-GIT
=

```   
     |\_/|                  
     | @ @|   GIT! 
     |   <>|              _  
     |  _/\|------___    | |
     |               `---' |   
 ____|_       ___|   |___.' 
/_/_____/____/_______|
```

**DO-GIT** is a class project made by Arthur and Matéo. It aims to set up a Git server containers using Docker to simulate communications between server and clients.


### Prequisites

• First, you'll need to install Docker on your machine :
_(On Debian based distro) :_
$ `sudo apt install docker.io`

• Next, download latest debian docker version :
$ `docker pull debian:latest`

• Then, you'll need to install Docker-Compose to execute the .yml :
_(We used a specific version in order to make our .yml working)_ 
$ ``sudo curl -L https://github.com/docker/compose/releases/download/1.25.3/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose``

$ `sudo chmod +x /usr/local/bin/docker-compose`

• To clone DO-GIT :
$ `git clone https://forge.univ-lyon1.fr/p2313767/do-git.git`


### Using DO-GIT

> Since we use scripts to transfere SSH key trough a shared volume (/DO-GIT/shared_key/) you will need to be a sudoer.

• Using Docker, we first start our docker-compose.yml :
$ `cd path/to/dog-it`
$ `docker-compose up`

• **In a new terminal**, now start either ALICE or BOB container :
$ `docker exec -it ALICE bash"`

• Start a new project :
$ `git init`

• Configure your name and your mail :
$ `git config --global user.email "you@example.com"`
$ `git config --global user.name "Your Name"`

• Then link to the SERVER container :
$ `git remote add origin ssh://git@172.20.0.5/home/git/repos/projet1`

• Few commands you should use by order :

| Commands | Function |
|:-|:-:|
| $ `touch your_file` | Create a file |
| $ `git add your_file` |  Add changes |    
| $ `git commit -m "save name"` | Save them |
| $ `git push --set-upstream origin master` | Create branch |
| $ `git push` | Send them |
| $ `git log` | Show log |

• If you want to work on the same project on the other container you could use this to get your branch :
*(You'll need to use the previous steps to connect to the other container, then to init, and to remotly connect)*
$ `git pull origin master`

### How does it works ?

##### dockerfiles
We created two docker files : one for the git SERVER container and the other one for ALICE and BOB containers.

• In the SERVER dockerfile :
It setup the right distro (Debian's Docker image that we downloaded previously). It download and install openssh-server and git. It automaticaly genereate RSA SSH keys. Then it create git user. FInnaly, it create anything that is needed to setup and use git.

• In the ALICE and BOB dockerfile :
It setup debian distro then it download and install openssh-client and git.


##### docker-compose.yml
Then we created a docker-compose.yml file to give the right instructions to Docker in order to create containers and execute commands and scripts in the right order (with depends[_](https://www.youtube.com/watch?v=dQw4w9WgXcQ)on).
It establish a virtual network and bind static IP address to the containers. It also provide the rights ports redirections for every containers. Then it successfully copied public SSH key from ALICE and BOB contenairs in the correct /home/git/.ssh/authorized_keys


### Area of improvement

We obviously could improve SSH key management by implementing a more secure procedure for storing and distributing public and private keys. This could include the use of a secret manager to automate and secure key distribution. We also could optimize Docker container configuration to improve performance, in particular by adjusting memory and CPU parameters as needed, or by using light distros like AlpineOS.
